﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace CSharpSQLiteCreateTable
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var connection = new SQLiteConnection("Data Source=Sample.db"))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "CREATE TABLE IF NOT EXISTS Sample( Name TEXT)";
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }
    }
}
